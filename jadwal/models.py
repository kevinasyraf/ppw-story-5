from django.db import models
import datetime

# Create your models here.
class Jadwal(models.Model):
    hari = models.CharField(max_length=10)
    tanggal = models.DateField()
    jam = models.TimeField()
    kegiatan = models.CharField(max_length = 50)
    tempat = models.CharField(max_length = 50)
    kategori = models.CharField(max_length = 50)

    def __str__(self):
        return self.kegiatan
