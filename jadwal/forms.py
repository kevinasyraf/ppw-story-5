from django import forms

from .models import Jadwal

class JadwalForm(forms.ModelForm):
  class Meta:
    model = Jadwal
    fields = ['hari', 'tanggal', 'jam', 'kegiatan', 'tempat', 'kategori']
    labels = {
        'hari':'Hari',
        'tanggal':'Tanggal',
        'jam':'Jam',
        'kegiatan':'Kegiatan',
        'tempat':'Tempat',
        'kategori':'Kategori'
    }

    widgets = {
        'hari': forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Masukkan hari kegiatan'
            },
        ),
        'tanggal':forms.DateInput(
            attrs={
                'class':'form-control',
                'placeholder':'yyyy-mm-dd'
            }
        ),
        'jam':forms.DateInput(
            attrs={
                'class':'form-control',
                'placeholder':'HH:MM'
            },
        ),
        'kegiatan':forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Masukkan nama kegiatan',
            },
        ),
        'tempat': forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Masukkan tempat kegiatan',
            },
        ),
        'kategori': forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Masukkan kategori kegiatan',
            },
        ),
    }

