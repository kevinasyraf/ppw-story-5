from django.urls import path

from . import views

urlpatterns = [
	path('', views.jadwal),
  path('<int:delete_id>/delete', views.delete, name='delete')

]