from django.shortcuts import render, redirect
from .models import Jadwal
from .forms import JadwalForm

def jadwal(request):
  jadwal = Jadwal.objects.all()
  form = JadwalForm(request.POST or None)
  if form.is_valid():
    form.save()
    return redirect('jadwal')  

  context = {
    'jadwal' : jadwal,
    'form' : form,
  }	

  return render(request, 'jadwal.html', context)

def buat_jadwal(request):
    form = JadwalForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('jadwal')  

    context = {
        'form':form
    }

    return render(request, 'jadwal.html', context)

def delete(request, delete_id):
    Jadwal.objects.filter(id=delete_id).delete()
    return redirect('jadwal')
